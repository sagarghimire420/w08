(function ($) {
    
      $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
      $('#btnAjax').click(function () { callRestAPI() });//call function callRESTAPI
    
      // Perform an asynchronous HTTP (Ajax) API request.
      function callRestAPI() {
        var root = 'https://jsonplaceholder.typicode.com';
        //Passing object with object
        $.ajax({
          url: root + '/posts/1',
          method: 'GET'
        }).then(function (response) {
          console.log(response);
          $('#showResult').html(response.body);
        });
      }
    })($);
    